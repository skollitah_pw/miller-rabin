import unittest

import main


class TestBlumBlumShub(unittest.TestCase):

    def test_miller_rabin(self):
        primes = [n for n in range(1, 100) if main.miller_rabin(n, 10)]
        self.assertEqual(primes,
                         [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89,
                          97])
        self.assertEqual(True, main.miller_rabin(997, 10))
        self.assertEqual(False, main.miller_rabin(993, 10))
        self.assertEqual(True, main.miller_rabin(661, 4))
        self.assertEqual(False, main.miller_rabin(663, 4))
        self.assertEqual(True, main.miller_rabin(317, 4))


if __name__ == '__main__':
    unittest.main()
